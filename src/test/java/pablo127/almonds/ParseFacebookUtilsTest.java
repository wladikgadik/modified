package pablo127.almonds;

import org.junit.After;
import org.junit.Before;
/**
 * 
 * @author Paweł Świderski (<a href="http://cv-pabloo.rhcloud.com/" target="_blank">http://cv-pabloo.rhcloud.com/</a>)
 */
public class ParseFacebookUtilsTest{

	@Before
	public void setUp() throws Exception {
		Parse.initialize("B3jrmh2W9yIzYCjOLHLI5tLcApvFl1NJAoiJMnTP", "eHDcUJ9DJJqrIsccHbBUFjb0d3srvtnbAJoQxhLL");
		ParseFacebookUtils.initialize("551918191574648");
	}

	@After
	public void tearDown() throws Exception {
	}

	/* TODO it's not working at that time
	@Test
	public void test() {
		List<String> permissions = Arrays.asList("email", "public_profile", "user_friends", "user_birthday",
				"user_location");
		ParseFacebookUtils.logIn(permissions, new LogInCallback() {
			
			public void done(ParseUser obj, ParseException e) {
				assertEquals(null, e);
				assertNotEquals(null, obj);
			}
		});
	}
	*/

}
